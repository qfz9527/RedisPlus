package com.maxbill;

import com.maxbill.fxui.root.LoadWindow;
import com.maxbill.fxui.root.RootWindow;
import com.sun.javafx.application.LauncherImpl;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 主程序
 *
 * @author MaxBill
 * @date 2019/07/06
 */
@SpringBootApplication
@MapperScan("com.maxbill.base.mapper")
public class MainApplication extends RootWindow {

    public static void main(String[] args) {
        //LauncherImpl.launchApplication(MainApplication.class, LoadWindow.class, args);
       launch(args);
    }

}
