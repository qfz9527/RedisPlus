package com.maxbill.fxui.util;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class CommonFxuiUtil {


    /**
     * 创建树组件图标
     *
     * @param iconUrl 图标地址
     * @return 图像组件
     */
    public static ImageView getNodeIcon(String iconUrl) {
        ImageView treeIcon = new ImageView();
        treeIcon.setImage(new Image(iconUrl));
        treeIcon.setFitWidth(15);
        treeIcon.setFitHeight(15);
        return treeIcon;
    }

}
