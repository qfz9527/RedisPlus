package com.maxbill.base.service;

import com.maxbill.base.bean.Connect;

import java.util.List;

public interface ConnectService {

    void initConnectData();

    List<Connect> selectConnectList();

}
