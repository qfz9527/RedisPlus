package com.maxbill.base.service;

import com.maxbill.base.mapper.DataMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 数据业务层
 *
 * @author MaxBill
 * @date 2019/07/06
 */
@Service
public class DataServiceImpl implements DataService {

    @Autowired
    private DataMapper dataMapper;

    /**
     * 配置连接信息
     */
    @Override
    public void initSystemData() {
        try {
            int tableCount1 = this.dataMapper.isExistsTable("T_CONNECT");
            if (tableCount1 == 0) {
                this.dataMapper.createConnectTable();
            }
            int tableCount2 = this.dataMapper.isExistsTable("T_SETTING");
            if (tableCount2 == 0) {
                this.dataMapper.createSettingTable();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
